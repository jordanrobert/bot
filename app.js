// to do: read <title> of links?, 'gay', change to hosts, mongodb, bandwagon for repeated text
// .g search, .feature suggestion, .rlog stuff, save logs

var irc = require('irc');
var request = require('request'); // GET-ing pages
var twilio = require('twilio'); // for sms texting
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

var url = 'mongodb://localhost:27017/test';
var CHR_BOLD = String.fromCharCode(0x02);
var GOOGLE_API_KEY = 'AIzaSyDuKK-y1cdZnp2SjD_o6kPrgJuSWvBz2vQ';
var GOOGLE_CX = '003964663043083769437:r7l7id37xmo';
var CHAN = '#dt';
var NICK = 'gaybot';
var USERS = [
	{
		alias: 'sour',
		username: '~sour',
		weatherLocation: '90013',
		phone: '+18189139507',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'jsz',
		username: '~jsz',
		weatherLocation: '28078',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'akara',
		username: '~Akara',
		weatherLocation: '92129',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'ario',
		username: '~ario',
		weatherLocation: 'Toronto Canada',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'sham',
		username: '~sham',
		weatherLocation: '02199',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'jan',
		username: '~jannnn',
		weatherLocation: '25701',
		phone: '+13045213581',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'mazgurth',
		username: '~nihil',
		weatherLocation: 'Helsinki, Finnland',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'Ralith',
		username: '~ralith',
		weatherLocation: 'Seattle WA',
		messages: [],
		hasNewMessages: false
	},
	{
		alias: 'pdc',
		username: '~pdc',
		weatherLocation: '95051',
		messages: [],
		hasNewMessages: false
	}
];
// whether or not the bot is turned on
var stateActive = true;
// rate limits
var weatherPingCount = smsPingCount = 0;
setInterval(function() {
	weatherPingCount = 0;
	smsPingCount = 0;
}, 60000);
var twilioClient = new twilio.RestClient('ACc987ed53f6a921f553ee572c0bc55f76', 'a5e587472950a7bdb8824685a2d3217f');
var ircClient = new irc.Client('irc.synirc.com', NICK, {
	username: 'gay',
	realName: 'not gay',
	channels: [CHAN],
	debug: true,
	showErrors: true
});

MongoClient.connect(url, function(mongoError, db) {
	ircClient.addListener('raw', function (data) {
		if(data.command == 'PRIVMSG' && data.args[0] == CHAN) {
			// where we store the user's object
			var from = data.nick;
			var message = data.args[1];
			var userObj = getUserObj('username', data.user);

			// admin commands
			if(from === 'sour') {
				// shut 'er down
				if(message == '.off') {
					sendMessage('OFF', 'Cya, losers!');
					stateActive = false;
				}

				// turn 'er on
				else if(message == '.on') {
					sendMessage('ON', 'Wuz goodington?');
					stateActive = true;
				}
			}
			
			// user commands
			if(stateActive) {
				// weather lookup
				// .w <area>
				if(/^\.w( .+)?$/.test(message)) {
					if(weatherPingCount >= 8) {
						sendMessage('WEATHER', 'Too many requests. Please wait.');
					} else {
						// if they gave a query; otherwise, use their weather default
						var query = escape(message.length > 3 ? message.substr(3) : userObj.weatherLocation);
						weatherPingCount++;

						if(query) {
							request('http://api.wunderground.com/api/2d088da75be34d64/conditions/q/' + query + '.json', function(error, response, html) {
								var json = JSON.parse(html);
								var weather = json.current_observation;

								if(weather) {
									if(userObj && userObj.weatherUnit == 'C') {
										var currentTemp = weather.temp_c + ' C';
										var feelsLikeTemp = weather.feelslike_c + ' C';
									} else {
										var currentTemp = weather.temp_f + ' F';
										var feelsLikeTemp = weather.feelslike_f + ' F';
									}
									sendMessage('WEATHER', weather.weather + ' ' + weather.temp_f + 'F / ' + weather.temp_c + 'C, feels like ' + weather.feelslike_f + 'F / ' + weather.feelslike_c + 'C in ' + weather.display_location.full);
								} else {
									sendMessage('WEATHER', 'Error.');
								}
							});
						}
					}
				}

				// google search
				// .g <search query>
				else if(/^\.g .+/.test(message)) {
					var query = escape(message.substr(3));
					request('https://www.googleapis.com/customsearch/v1?cx=' + GOOGLE_CX + '&q=' + query + '&key=' + GOOGLE_API_KEY, function(error, response, html) {
						var json = JSON.parse(html);
						
						if(json.items.length) {
							sendMessage('GOOGLE', json.items[0].title + ' - ' + json.items[0].link);
						} else {
							sendMessage('GOOGLE', 'Error.');
						}
					});
				}

				// youtube search
				// .y <search query>
				else if(/^\.y .+/.test(message)) {
					var query = escape(message.substr(3));
					request('https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=' + query + '&maxResults=1&key=' + GOOGLE_API_KEY, function(error, response, html) {
						var json = JSON.parse(html);
						
						if(!json.error && json.items.length) {
							sendMessage('YOUTUBE', json.items[0].snippet.title + ' - https://www.youtube.com/watch?v=' + json.items[0].id.videoId);
						} else {
							sendMessage('YOUTUBE', 'Error.');
						}
					});
				}

				// youtube scanner
				// https://www.youtube.com/watch?v=lw3Or6eqIpI
				else if(message.match(/youtube\.com\/watch\?/) || message.match(/youtu.be/)) {
					var videoIdMatch = message.match(/v=([^&]+)(&.+?)?/) || message.match(/youtu.be\/([^\?]+)/);
					if(videoIdMatch) {
						var videoId = videoIdMatch[1];
						request('https://www.googleapis.com/youtube/v3/videos?part=snippet&id=' + videoId + '&key=' + GOOGLE_API_KEY, function(error, response, html) {
							var json = JSON.parse(html);
							
							if(!json.error && json.items.length) {
								sendMessage('YOUTUBE', json.items[0].snippet.title);
							} else {
								sendMessage('YOUTUBE', 'Error.');
							}
						});
					}
				}

				// texting
				else if(message.match(/^\.text .+/)) {
					if(userObj.phone) {
						if(smsPingCount >= 5) {
							sendMessage('TEXT', 'Too many requests. Please wait.');
						} else {
							var query = message.substr(6);
							smsPingCount++;

							// if it looks like a link, let's try to shorten it
							// because long texts don't send
							if(message.match(/^\.text (http|www)/)) {
								request('https://api-ssl.bitly.com/v3/shorten?access_token=9bc43e4c3dc2d1fc5e48dd1cfd792a45b7286c2c&longUrl=' + encodeURIComponent(query), function(error, response, html) {
									var json = JSON.parse(html);
									query = 'http://bit.ly/' + json.data.hash;
									sendSMSText();
								});
							} else {
								sendSMSText();
							}

							function sendSMSText() {
								twilioClient.sms.messages.create({
								    to:userObj.phone,
								    from:'+18186964707',
								    body: query
								});
							}

							sendMessage('TEXT', 'Sent successfully.');
						}
					}
				}

				// save message
				else if(message.match(/^\.message .+/)) {
					var recipient = message.split(' ')[1];
					var recipientObj = getUserObj('alias', recipient);
					var content = message.substr(recipient.length + 10);
					if(recipientObj) {
						recipientObj.messages.push({
							from: from,
							message: content
						});
						recipientObj.hasNewMessages = true;
						sendMessage('MESSAGE', 'Message stored.');
					} else {
						sendMessage('MESSAGE', 'I don\'t recognize that alias.');
					}
				}

				// read messages
				else if (message === '.messages') {
					if(userObj.messages.length) {
						userObj.messages.forEach(function(item) {
							sendMessage('MESSAGE', '<' + item.from + '> ' + item.message);
						});
						userObj.hasNewMessages = false;
						userObj.messages = [];
					} else {
						sendMessage('MESSAGE', 'You have no new messages.');
					}
				}

				// cringe
				else if (message.match(/^\.cringe.*/)) {
					var filter = { message: { $regex: /^.{10,}$/ } };
					var count = db.collection('logs').count(filter, function(err, numberOfDocs) {
						var randomIndex = Math.floor(Math.random() * numberOfDocs + 1);
						var documents = db.collection('logs').find(filter).limit(1).skip(randomIndex);
						documents.each(function(err, doc) {
							if (doc != null) sendMessage('CRINGE', '[' + doc.date + '] <' + doc.nick + '> ' + doc.message);
						});
					});
				}

				// rslog
				else if (message.match(/^\.rslog .+/)) {
					var query = message.substr(7);
					
				}
			}

			// message notifications
			if(userObj && userObj.hasNewMessages) {
				sendMessage('MESSAGE', from + ', you have unread messages. Type \'.messages\' to view them.');
				userObj.hasNewMessages = false;
			}
		}
	});
});


function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
}

// get a user object
function getUserObj(key, match) {
	var userObj = false;
	USERS.forEach(function(item) {
		if(item[key] === match) {
			userObj = item;
		}
	});

	return userObj;
}

function sendMessage(heading, body) {
	ircClient.say(CHAN, CHR_BOLD + heading + CHR_BOLD + ': ' + body);
}